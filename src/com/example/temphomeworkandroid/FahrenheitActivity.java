package com.example.temphomeworkandroid;

import conversions.Fahrenheit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FahrenheitActivity extends Activity
{
	private Fahrenheit 	f;

	private Button 		submit;
	private EditText 	et1;
	private TextView 	errorText;
	
	private static int F_CODE = 1;
	
	private Intent i = new Intent();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fahrenheit);

		i = getIntent();
		
		et1 			= (EditText) findViewById(R.id.editText);
		submit 			= (Button) findViewById(R.id.convert_button);
		errorText 		= (TextView)findViewById(R.id.err_msg);
		
		submit	.setOnClickListener(new MyListener());

	}

	private class MyListener implements OnClickListener
	{

		@Override
		public void onClick(View v)
		{	
			if(v.getId() == R.id.convert_button)
			{
				if(et1.getText().toString().length() == 0)
				{
					errorText.setText("Sorry, Please enter a temperature.");
					return;
				}
				
				f = new Fahrenheit(et1.getText().toString());
				
				i.putExtra("celcius_temp", f.convert() + "C�");
				
				setResult(F_CODE, i);
				finish(); }}}}
