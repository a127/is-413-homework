package com.example.temphomeworkandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity
{

	private Button 		f_to_c;
	private Button 		c_to_f;
	private EditText 	temperature;
	private TextView 	display_temperature;
	
	private Intent i;
	
	private static final int F_CODE 	= 1;
	private static final int C_CODE 	= 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		f_to_c 					= (Button)findViewById(R.id.f_to_c);
		c_to_f 					= (Button)findViewById(R.id.c_to_f);
		display_temperature 	= (TextView)findViewById(R.id.display_temperature);
	
		
		f_to_c				.setOnClickListener(new SelectConversionListener());
		c_to_f				.setOnClickListener(new SelectConversionListener());
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	
		if(resultCode != requestCode || data == null)
		{
			display_temperature.setText("Sorry, an error occured");
		}
		
		else if(resultCode == F_CODE)
		{
			display_temperature.setText(data.getStringExtra("celcius_temp"));	
		}
		else if(resultCode == C_CODE)
		{
			display_temperature.setText(data.getStringExtra("fahrenheit_temp"));
		}
		else
		{
			display_temperature.setText("Sorry, an error has occurred");
			return;
		}
		
	}
	
	private class SelectConversionListener implements OnClickListener
	{

		@Override
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.f_to_c:
					i = new Intent(MainActivity.this, FahrenheitActivity.class);
					startActivityForResult(i, F_CODE);
				break;
				
				case R.id.c_to_f:
					i = new Intent(MainActivity.this, CelciusActivity.class);
					startActivityForResult(i, C_CODE);		
				break;
					
				
			}
			
			
		}
		
		
	}
	
}
